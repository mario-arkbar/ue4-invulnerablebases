/*

ARK Server Plugin
Invulnerable Bases

Makes player structures either completely invulnerable or vulnerable only at given times.

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#include "../InvulnerableBasesPrivatePCH.h"

Configuration::Configuration()
{
	Enabled = true;
	ProtectedModifier = 0.0f;
	UnprotectedModifier = 1.0f;

	UnprotectedDays[0] = true;
	for (int i = 1; i <= 8; i++)
		UnprotectedDays[i] = true;
}


void Configuration::Load()
{
	char buffer[4069], settingsPath[MAX_PATH];
	string strResult;
	int iEnabled1 = -1, iEnabled2 = -1;


	// Get .ini file location
	GetModuleFileNameA(NULL, settingsPath, MAX_PATH);
	PathRemoveFileSpecA(settingsPath);
	strcat_s(settingsPath, MAX_PATH, "\\plugins\\");
	strcat_s(settingsPath, MAX_PATH, SETTINGS_PATH);


	// Get Plugin Enabled state
	// ** Old
	if (GetPrivateProfileStringA("TOGGLE", "Enabled", "-1", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			iEnabled1 = atoi(strResult.c_str());
		}
		catch (...){}
	}

	// ** New
	if (GetPrivateProfileStringA("SETTINGS", "Enabled", "-1", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			iEnabled2 = atoi(strResult.c_str());
		}
		catch (...){}
	}

	// ** If both values <= 0, then disable
	Enabled = (iEnabled1 > 0) || (iEnabled2 > 0);

	// Get Allow Admins to do structure damage
	if (GetPrivateProfileStringA("SETTINGS", "AllowAdminStructureDamage", "0", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			AllowAdminStructureDamage = atoi(strResult.c_str()) == 1;
		}
		catch (...){}
	}

	// Get Unprotected Days
	if (GetPrivateProfileStringA("WEEKDAYS", "UnprotectedDays", "", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		if (strResult.size())
		{
			UnprotectedDays[0] = false;
			istringstream iss(strResult);
			try
			{
				int DayOfWeek;
				while (iss >> DayOfWeek)
					UnprotectedDays[DayOfWeek] = true;
			}
			catch (...){}
		}
		else
		{
			// If UnprotectedDays is empty, only check against StartHour[0] and StopHour[0]
			UnprotectedDays[0] = true;
		}
	}

	// Get General Start Time
	if (GetPrivateProfileStringA("TIME", "StartHour", 0, buffer, sizeof(buffer), settingsPath) || GetPrivateProfileStringA("PROTECTIONTIME", "StartHour", 0, buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			StartHour[0] = atoi(strResult.c_str());
		}
		catch (...){}
	}


	// Get General Stop Time
	if (GetPrivateProfileStringA("TIME", "StopHour", 0, buffer, sizeof(buffer), settingsPath) || GetPrivateProfileStringA("PROTECTIONTIME", "StopHour", 0, buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			StopHour[0] = atoi(strResult.c_str());
		}
		catch (...){}
	}


	// Get individual Start and Stop Times for Unprotected Days
	char section[MAX_PATH];
	for (int i = 1; i <= 7; i++)
	{
		sprintf_s(section, "PROTECTIONTIME%d", i);

		// Get indivdiual Start Time. If setting does not exist, set it to 0xFF. It will then be assigned the default value (StartHour[0]).
		if (GetPrivateProfileStringA(section, "StartHour", "255", buffer, sizeof(buffer), settingsPath))
		{
			strResult = string(buffer);
			try
			{
				StartHour[i] = atoi(strResult.c_str());
				if (StartHour[i] == 0xFF)
				{
					StartHour[i] = StartHour[0];
					//UnprotectedDays[i] = false;
				}
			}
			catch (...){}
		}


		// Get indivdiual Stop Time. If setting does not exist, set it to 0xFF. It will then be assigned the default value (StopHour[0]).
		if (GetPrivateProfileStringA(section, "StopHour", "255", buffer, sizeof(buffer), settingsPath))
		{
			strResult = string(buffer);
			try
			{
				StopHour[i] = atoi(strResult.c_str());
				if (StopHour[i] == 0xFF)
				{
					StopHour[i] = StopHour[0];
					//UnprotectedDays[i] = false;
				}
		}
			catch (...){}
		}
	}


	// Get Protected Damage Modifier
	if (GetPrivateProfileStringA("DAMAGE", "ProtectedModifier", "0.0", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		size_t szResult;
		try
		{
			ProtectedModifier = stof(strResult, (size_t*)&szResult);
		}
		catch (...){}
	}


	// Get Unprotected Damage Modifier
	if (GetPrivateProfileStringA("DAMAGE", "UnprotectedModifier", "1.0", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		size_t szResult;
		try
		{
			UnprotectedModifier = stof(strResult, (size_t*)&szResult);
		}
		catch (...){}
	}

	// Do Broadcast on Vulnerability Change?
	if (GetPrivateProfileStringA("BROADCAST", "DoBroadcast", "1", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			BroadcastOnVulnerabilityChange = atoi(strResult.c_str()) == 1;
		}
		catch (...){}
	}

	// Get Broadcast Messages on Vulnerability Change ON
	if (GetPrivateProfileStringA("BROADCAST", "MessageProtectionOn", "\\n\\n** ATTENTION! **\\n\\nAsset Protection is now ON!", buffer, sizeof(buffer), settingsPath))
	{
		BroadcastMessageProtectionOn = string(buffer);
	}

	// Get Broadcast Messages on Vulnerability Change oFF
	if (GetPrivateProfileStringA("BROADCAST", "MessageProtectionOff", "\\n\\n** ATTENTION! **\\n\\nAsset Protection is now OFF!", buffer, sizeof(buffer), settingsPath))
	{
		BroadcastMessageProtectionOff = string(buffer);
	}

}


void Configuration::GetHookProcInfo(char *Identifier, string &Pattern, string &Mask) {

	char buffer[MAX_PATH], settingsPath[MAX_PATH];
	string strResult;

	// Get pattern file location
	GetModuleFileNameA(NULL, settingsPath, MAX_PATH);
	PathRemoveFileSpecA(settingsPath);
	strcat_s(settingsPath, MAX_PATH, "\\plugins\\");
	strcat_s(settingsPath, MAX_PATH, PATTERN_PATH);

	Pattern = "";
	Mask = "";

	// Get pattern
	if (GetPrivateProfileStringA(Identifier, "Pattern", "", buffer, sizeof(buffer), settingsPath)) {
		Pattern = string(buffer);
		//strncpy(Pattern, buffer, strlen(buffer));
	}

	// Get mask
	if (GetPrivateProfileStringA(Identifier, "Mask", "", buffer, sizeof(buffer), settingsPath)) {
		Mask = string(buffer);
		//strncpy(Mask, buffer, strlen(buffer));
	}

}
