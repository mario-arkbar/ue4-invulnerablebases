/*

ARK Server Plugin
Invulnerable Bases

Makes player structures either completely invulnerable or vulnerable only at given times.

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#include "../InvulnerableBasesPrivatePCH.h"

#pragma region Declarations

#define TIMER_INTERVAL	10000	// 10s

typedef DWORD64	APrimalStructure;
typedef DWORD64* AShooterPlayerController;

// - Helpers ---------------------------------------------------------------------------------------------------------------------------

DWORD64 dwUWorld = NULL;
HANDLE hTimerQueue = NULL;
bool bLastVulnerabilityState = false;

bool StartTimer();
void StopTimer();
void CALLBACK TimerFunction(PVOID, BOOLEAN);
bool IsProtectedHour(int, int, int);
bool IsProtected();

DWORD64(__cdecl *APlayerController_ConsoleCommand)(DWORD64, FString*, FString*, bool);
DWORD64(__cdecl *UWorld_GetFirstPlayerController)(DWORD64);


// - void __cdecl UWorld::InitWorld(UWorld *this, UWorld::InitializationValues IVS) ---------------------------------------------------
typedef void(WINAPI *UWorld_InitWorld)(DWORD64, DWORD64);
void WINAPI detour_UWorld_InitWorld(DWORD64, DWORD64);
UWorld_InitWorld oUWorld_InitWorld = NULL;

// - float APrimalStructure::TakeDamage(APrimalStructure *this, float Damage, FDamageEvent *DamageEvent, AController *EventInstigator, AActor *DamageCauser)
typedef float(WINAPI *PrimalStructure_TakeDamage)(APrimalStructure*, float, FDamageEvent*, AController*, AActor*);
float WINAPI detour_PrimalStructure_TakeDamage(APrimalStructure*, float, FDamageEvent*, AController*, AActor*);
PrimalStructure_TakeDamage oPrimalStructure_TakeDamage = NULL;


// - Hookers --------------------------------------------------------------------------------------------------------------------------

Hook<PrimalStructure_TakeDamage> hookTakeDamage;
Hook<UWorld_InitWorld> hookInitWorld;

#pragma endregion 


#pragma region Helper

inline bool fileExists(const std::string& name) {
	ifstream f(name.c_str());
	if (f.good()) {
		f.close();
		return true;
	}
	else {
		f.close();
		return false;
	}
}

void CALLBACK TimerFunction(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	HANDLE hTimer = NULL;
	HANDLE hTimerQueue = (HANDLE)lpParam;

	if (configuration.Enabled && configuration.BroadcastOnVulnerabilityChange)
	{
		bool bCurrentProtectionState = IsProtected();
		if (bCurrentProtectionState != bLastVulnerabilityState)
		{
#ifdef DEBUG
			char s[MAX_PATH];
			sprintf(s, "[DEBUG] Vulnerability state changed %d -> %d", bLastVulnerabilityState, bCurrentProtectionState);
			PluginAPI::ConsoleMessage(s);
#endif
			bLastVulnerabilityState = bCurrentProtectionState;

			// If we got the World ...
			if (dwUWorld)
			{
				// ... get the first available APlayerController
				DWORD64 playerController = UWorld_GetFirstPlayerController(dwUWorld);

				// If we got the APlayerController ...
				if (playerController)
				{
					char m[4096];
#ifdef DEBUG
					sprintf(m, "[DEBUG] Broadcasting message using APlayerController 0x%x", playerController);
					PluginAPI::ConsoleMessage(m);
#endif

					// Prepare the data
					if (bCurrentProtectionState)
						sprintf_s(m, "broadcast %s                              ", configuration.BroadcastMessageProtectionOn.c_str());
					else
						sprintf_s(m, "broadcast %s                              ", configuration.BroadcastMessageProtectionOff.c_str());

					PluginAPI::ConsoleMessage(m);

					FString result;
					FString command(m);

					// Send the Broadcast!
					APlayerController_ConsoleCommand(playerController, &result, &command, false);
				}
#ifdef DEBUG
				else {
					char s[MAX_PATH];
					sprintf(s, "[DEBUG] Could not find APlayerController with UWorld 0x%x", dwUWorld);
					PluginAPI::ConsoleMessage(s, ConsoleAPI::LOG_WARNING);
				}
#endif

			}
		}

	}

	/* Testing if we can get a player's admin status
	if (dwUWorld)
	{
		DWORD64 playerController = UWorld_GetFirstPlayerController(dwUWorld);
		if (playerController)
		{
			DWORD64 *dwIsAdmin = (DWORD64*)(playerController + 0x7E0);
			bool bIsAdmin = (*dwIsAdmin == 4);
			char s[32];
			sprintf_s(s, "IsAdmin (0x%x): %d %d", &dwIsAdmin, *dwIsAdmin, bIsAdmin);
			PluginAPI::ConsoleMessage(s);
		}
	}
	*/

	CreateTimerQueueTimer(&hTimer, hTimerQueue, (WAITORTIMERCALLBACK)TimerFunction, hTimerQueue, TIMER_INTERVAL, 0, 0);
}


bool StartTimer()
{
	HANDLE hTimer = NULL;

	hTimerQueue = CreateTimerQueue();
	if (NULL == hTimerQueue)
		return false;

	if (!CreateTimerQueueTimer(&hTimer, hTimerQueue, (WAITORTIMERCALLBACK)TimerFunction, hTimerQueue, TIMER_INTERVAL, 0, 0))
		return false;

#ifdef DEBUG
	PluginAPI::ConsoleMessage("[DEBUG][StartTimer] Timer started");
#endif

	return true;
}


void StopTimer()
{
	if (hTimerQueue)
		DeleteTimerQueue(hTimerQueue);
}


bool IsProtectedHour(int current, int start, int stop)
{
	if ((start == -1) && (stop == -1))
		return false;

	if (start < stop)
	{
		return start <= current && current < stop;
	}
	return start <= current || current < stop;
}


bool IsProtected()
{
	bool bIsProtected = true;

	if (configuration.Enabled) {
		int iCurrentDayOfWeek;
		time_t now = time(NULL);
		tm now_tm;
		localtime_s(&now_tm, &now);

		// Check if we have 7-days Protection
		if (configuration.UnprotectedDays[0])
		{
			// Check if we are in Protected Time
			bIsProtected = IsProtectedHour(now_tm.tm_hour, configuration.StartHour[0], configuration.StopHour[0]);
		}
		else
		{
			// Check if we are in an Unprotected day
			iCurrentDayOfWeek = now_tm.tm_wday;
			if (iCurrentDayOfWeek == 0)
				iCurrentDayOfWeek = 7;
			if (configuration.UnprotectedDays[iCurrentDayOfWeek])
				bIsProtected = IsProtectedHour(now_tm.tm_hour, configuration.StartHour[iCurrentDayOfWeek], configuration.StopHour[iCurrentDayOfWeek]);
		}
	}

	return bIsProtected;
}

#pragma endregion

#pragma region Invulnerable Bases Plugin

void Plugin::Load()
{
	char m[MAX_PATH];
	string strHookPattern, strHookMask;
		
	sprintf_s(m, "%s :: Version %s", PLUGIN_NAME, PLUGIN_VERSION);
	PluginAPI::ConsoleMessage(m);



#pragma region UWorld::InitWorld
	configuration.GetHookProcInfo("InitWorld", strHookPattern, strHookMask);

#ifdef DEBUG
	sprintf_s(m, "[DEBUG][Hook Info][%s] Pattern (%d): %s", "InitWorld", strHookPattern.length(), strHookPattern.c_str());
	PluginAPI::ConsoleMessage(m);
	sprintf_s(m, "[DEBUG][Hook Info][%s] Mask (%d): %s", "InitWorld", strHookMask.length(), strHookMask.c_str());
	PluginAPI::ConsoleMessage(m);
#endif

	hookInitWorld.Create(
		"\x48\x8B\xC4\x89\x50\x10\x55\x57",
		"xxxxxxxx",
		&detour_UWorld_InitWorld,
		&oUWorld_InitWorld
	);

	if (hookInitWorld.status == hookInitWorld.INITIALIZED) {
		hookInitWorld.Enable();

#ifdef DEBUG
		if (hookInitWorld.hookStatus == MH_OK) {
			sprintf_s(m, "[DEBUG][%s] Original function found at 0x%x", "hookInitWorld", hookInitWorld.Address());
			PluginAPI::ConsoleMessage(m);
		}
		else {
			sprintf_s(m, "[DEBUG][%s] Original function NOT found!", "hookInitWorld");
			PluginAPI::ConsoleMessage(m, ConsoleAPI::LOG_ERROR);
		}
#endif
	}
	else {
		sprintf_s(m, "Could not initialize %s! (%d)", "hookInitWorld", hookInitWorld.hookStatus);
		PluginAPI::ConsoleMessage(m, ConsoleAPI::LOG_ERROR);
	}
#pragma endregion



#pragma region APrimalStructure::TakeDamage
	configuration.GetHookProcInfo("TakeDamage", strHookPattern, strHookMask);

#ifdef DEBUG
	sprintf_s(m, "[DEBUG][Hook Info][%s] Pattern (%d): %s", "TakeDamage", strHookPattern.length(), strHookPattern.c_str());
	PluginAPI::ConsoleMessage(m);
	sprintf_s(m, "[DEBUG][Hook Info][%s] Mask (%d): %s", "TakeDamage", strHookMask.length(), strHookMask.c_str());
	PluginAPI::ConsoleMessage(m);
#endif

	hookTakeDamage.Create(
		"\x40\x55\x53\x56\x57\x41\x55\x41\x57\x48\x8D\xAC\x24\x00\x00\x00\x00\x48\x81\xEC\x00\x00\x00\x00\x8B\x99\x00\x00\x00\x00",
		"xxxxxxxxxxxxx????xxx????xx????",
		&detour_PrimalStructure_TakeDamage,
		&oPrimalStructure_TakeDamage
	);
	if (hookTakeDamage.status == hookTakeDamage.INITIALIZED) {
		hookTakeDamage.Enable();

#ifdef DEBUG
		if (hookTakeDamage.hookStatus == MH_OK) {
			sprintf_s(m, "[DEBUG][%s] Original function found at 0x%x", "hookTakeDamage", hookTakeDamage.Address());
			PluginAPI::ConsoleMessage(m);
		}
		else {
			sprintf_s(m, "[DEBUG][%s] Original function NOT found!", "hookTakeDamage");
			PluginAPI::ConsoleMessage(m, ConsoleAPI::LOG_ERROR);
		}
#endif
	}
	else {
		sprintf_s(m, "Could not initialize %s! (%d)", "hookTakeDamage", hookTakeDamage.hookStatus);
		PluginAPI::ConsoleMessage(m, ConsoleAPI::LOG_ERROR);
	}
#pragma endregion



#pragma region UWorld::GetFirstPlayerController
	configuration.GetHookProcInfo("GetFirstPlayerController", strHookPattern, strHookMask);

#ifdef DEBUG
	sprintf_s(m, "[DEBUG][Hook Info][%s] Pattern (%d): %s", "GetFirstPlayerController", strHookPattern.length(), strHookPattern.c_str());
	PluginAPI::ConsoleMessage(m);
	sprintf_s(m, "[DEBUG][Hook Info][%s] Mask (%d): %s", "GetFirstPlayerController", strHookMask.length(), strHookMask.c_str());
	PluginAPI::ConsoleMessage(m);
#endif

	UWorld_GetFirstPlayerController = (DWORD64(_cdecl*)(DWORD64))Pattern::Search(
		"\x40\x53\x48\x83\xEC\x20\x33\xDB\x39\x99\x00\x00\x00\x00",
		"xxxxxxxxxx????"
	);
#ifdef DEBUG
	sprintf_s(m, "[DEBUG][%s] Original function found at 0x%x", "UWorld::GetFirstPlayerController", &UWorld_GetFirstPlayerController);
	PluginAPI::ConsoleMessage(m);
#endif
#pragma endregion



#pragma region APlayerController::ConsoleCommand
	configuration.GetHookProcInfo("ConsoleCommand", strHookPattern, strHookMask);

#ifdef DEBUG
	sprintf_s(m, "[DEBUG][Hook Info][%s] Pattern (%d): %s", "ConsoleCommand", strHookPattern.length(), strHookPattern.c_str());
	PluginAPI::ConsoleMessage(m);
	sprintf_s(m, "[DEBUG][Hook Info][%s] Mask (%d): %s", "ConsoleCommand", strHookMask.length(), strHookMask.c_str());
	PluginAPI::ConsoleMessage(m);
#endif

	APlayerController_ConsoleCommand = (DWORD64(_cdecl*)(DWORD64, FString*, FString*, bool))Pattern::Search(
		"\x48\x8B\xC4\x44\x88\x48\x20\x4C\x89\x40\x18\x48\x89\x50\x10\x48\x89\x48\x08\x55\x53\x41\x54",
		"xxxxxxxxxxxxxxxxxxxxxxx"
	);
#ifdef DEBUG
	sprintf_s(m, "[DEBUG][%s] Original function found at 0x%x", "APlayerController::ConsoleCommand", &APlayerController_ConsoleCommand);
	PluginAPI::ConsoleMessage(m);
#endif
#pragma endregion



	LoadSettings();

	bLastVulnerabilityState = IsProtected();

	StartTimer();

	PluginAPI::ConsoleMessage("Plugin Loaded");
}


void Plugin::Unload()
{
	// Plugin does this when unloaded
	StopTimer();

	hookInitWorld.Disable();
	hookTakeDamage.Disable();

	PluginAPI::ConsoleMessage("Plugin Unloaded");
}


void Plugin::LoadSettings()
{
	char message[MAX_PATH];
	time_t now;
	tm now_tm;

	PluginAPI::ConsoleMessage("Loading settings ...");
	configuration.Load();

	now = time(NULL);
	localtime_s(&now_tm, &now);

	sprintf_s(message, "\tEnabled: %s", configuration.Enabled ? "YES" : "NO");
	PluginAPI::ConsoleMessage(message);


	if (configuration.Enabled)
	{
		sprintf_s(message, "\tCurrent server time: %02d:%02d", now_tm.tm_hour, now_tm.tm_min);
		PluginAPI::ConsoleMessage(message);

		if (configuration.UnprotectedDays[0])
		{
			if ((configuration.StartHour[0] == 0) && (configuration.StopHour[0] == 0))
			{
				PluginAPI::ConsoleMessage("\tProtection: 24/7");
			}
			else
			{
				PluginAPI::ConsoleMessage("\tProtection: 7 Days Per Week");

				sprintf_s(message, "\tProtection starts: %02d:00", configuration.StartHour[0]);
				PluginAPI::ConsoleMessage(message);

				sprintf_s(message, "\tProtection ends: %02d:00", configuration.StopHour[0]);
				PluginAPI::ConsoleMessage(message);
			}
		}
		else
		{
			char s[MAX_PATH];

			PluginAPI::ConsoleMessage("\tProtection Timeplan");

			sprintf_s(message, "\t  ");
			for (int i = 1; i <= 7; i++)
			{
				if (configuration.UnprotectedDays[i])
				{
					char *s = WeekDaysAbbr[i - 1];
					strcat_s(message, MAX_PATH, s);
					if (i < 7)
						strcat_s(message, MAX_PATH, "  ");
				}
			}
			PluginAPI::ConsoleMessage(message);

			sprintf_s(message, "\t+ ");
			for (int i = 1; i <= 7; i++)
			{
				if (configuration.UnprotectedDays[i])
				{
					int iStartHour = configuration.StartHour[i], iStopHour = configuration.StopHour[i];
					if (iStartHour < 0)
						sprintf_s(s, "NEVER");
					else if ((iStartHour == 0) && (iStopHour == 0))
						sprintf_s(s, "FULL ");
					else
						sprintf_s(s, "%02d:00", iStartHour);
					strcat_s(message, MAX_PATH, s);
					if (i < 7)
						strcat_s(message, MAX_PATH, "  ");
				}
			}
			PluginAPI::ConsoleMessage(message);

			sprintf_s(message, "\t- ");
			for (int i = 1; i <= 7; i++)
			{
				if (configuration.UnprotectedDays[i])
				{
					int iStartHour = configuration.StartHour[i], iStopHour = configuration.StopHour[i];
					if (iStopHour < 0)
						sprintf_s(s, "NEVER");
					else if ((iStartHour == 0) && (iStopHour == 0))
						sprintf_s(s, "FULL ");
					else
						sprintf_s(s, "%02d:00", iStopHour);
					strcat_s(message, MAX_PATH, s);
					if (i < 7)
						strcat_s(message, MAX_PATH, "  ");
				}
			}
			PluginAPI::ConsoleMessage(message);
		}

		sprintf_s(message, "\tProtected Damage Modifier: %f", configuration.ProtectedModifier);
		PluginAPI::ConsoleMessage(message);

		sprintf_s(message, "\tUnprotected Damage Modifier: %f", configuration.UnprotectedModifier);
		PluginAPI::ConsoleMessage(message);
	}


	if (fileExists("instance.uworld"))
	{
		FILE *f;
		f = fopen("instance.uworld", "rb");
		if (f) {
			fread(&dwUWorld, sizeof(DWORD64), 1, f);
			fclose(f);
#ifdef DEBUG
			sprintf(message, "[DEBUG] Restored UWorld instance address from file (0x%x)", dwUWorld);
			PluginAPI::ConsoleMessage(message);
#endif
		}
	}

	PluginAPI::ConsoleMessage("Settings loaded!");
}

#pragma endregion


float WINAPI detour_PrimalStructure_TakeDamage(APrimalStructure *PrimalStructure, float DamageAmount, FDamageEvent *DamageEvent, AController *EventInstigator, AActor *DamageCauser)
{
#ifdef DEBUG
	PluginAPI::ConsoleMessage("[DEBUG][APrimalStructure::TakeDamage] Disabling hook");
#endif
	hookTakeDamage.Disable();

	bool bIsProtected = false;
	float flOriginalDamageAmount = DamageAmount;

	// If enabled
	if (configuration.Enabled) {

		// Just testing if we can check if the damage causer is admin
		/*AShooterPlayerController *playerController = (AShooterPlayerController*)EventInstigator;
		if (playerController)
		{
			DWORD64 *dwIsAdmin = (DWORD64*)(playerController + 0x7E0);
			bool bIsAdmin = (*dwIsAdmin == 4);
			char s[32];
			sprintf_s(s, "TakeDamage->IsAdmin (0x%x): %d %d", &dwIsAdmin, *dwIsAdmin, bIsAdmin);
			PluginAPI::ConsoleMessage(s);
		}*/

		/*try
		{
			AShooterPlayerController *playerController = (AShooterPlayerController*)DamageCauser->GetInstigatorController();
			DWORD64 *dwIsAdmin = (DWORD64*)(playerController + 0x7E0);
			bool bIsAdmin = (*dwIsAdmin == 4);
			char s[32];
			sprintf_s(s, "TakeDamage->IsAdmin (0x%x): %d %d", &dwIsAdmin, *dwIsAdmin, bIsAdmin);
			PluginAPI::ConsoleMessage(s);
		}
		catch (...)
		{
			PluginAPI::ConsoleMessage("Exception caught!");
		}*/

		bIsProtected = IsProtected();

		// Apply modifiers according to Protection state
		if (bIsProtected)
			DamageAmount *= configuration.ProtectedModifier;
		else
			DamageAmount *= configuration.UnprotectedModifier;
	}

	// Call the original game function
	float lpResult = 0;
	try {
#ifdef DEBUG
		char m[MAX_PATH];
		sprintf_s(m, "[DEBUG][APrimalStructure::TakeDamage] Damage Old / New: %.4f / %.4f", flOriginalDamageAmount, DamageAmount);
		PluginAPI::ConsoleMessage(m);

		PluginAPI::ConsoleMessage("[DEBUG][APrimalStructure::TakeDamage] Calling original function");
#endif

		lpResult = oPrimalStructure_TakeDamage(PrimalStructure, DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	}
	catch (...) {
		PluginAPI::ConsoleMessage("[DEBUG][APrimalStructure::TakeDamage] Exception caught!", ConsoleAPI::LOG_ERROR);
	}

	// Enable the hook and unlock the mutex
	hookTakeDamage.Enable();
#ifdef DEBUG
	PluginAPI::ConsoleMessage("[DEBUG][APrimalStructure::TakeDamage] Enabling hook");
#endif

	return lpResult;
}

void WINAPI detour_UWorld_InitWorld(DWORD64 world, DWORD64 IVS)
{
#ifdef DEBUG
	PluginAPI::ConsoleMessage("[DEBUG][UWorld::InitWorld] Disabling hook");
#endif
	hookInitWorld.Disable();

	dwUWorld = world;

	// Save UWorld instance
	FILE *f;
	f = fopen("instance.uworld", "wb");
	if (f)
	{
#ifdef DEBUG
		char m[MAX_PATH];
		sprintf_s(m, "[DEBUG][UWorld::InitWorld] Saving world instance 0x%x", dwUWorld);
		PluginAPI::ConsoleMessage(m);
#endif
		fwrite(&dwUWorld, sizeof(DWORD64), 1, f);
		fclose(f);
	}

#ifdef DEBUG
	PluginAPI::ConsoleMessage("[DEBUG][UWorld::InitWorld] Calling original function");
#endif
	oUWorld_InitWorld(world, IVS);

#ifdef DEBUG
	PluginAPI::ConsoleMessage("[DEBUG][UWorld::InitWorld] Enabling hook");
#endif

	hookInitWorld.Enable();
}