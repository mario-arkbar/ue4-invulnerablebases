/*

ARK Server Plugin
TestPlugin

Practical Example on how to use ARK BAR

Copyright (c) 2015 Team ARK Bar
- Mario Werner			(Nachbars Lumpi)	<mario@108bits.de
- Daniel Gothenborg		(Xstasy)			<daniel@dgw.no>
- Alexander Hagabråten	(freakbyte)			<freakbyte@gmail.com>

*/

#include "../InvulnerableBasesPrivatePCH.h"

class Plugin
{
public:
	static void LoadSettings();
	static void Load();
	static void Unload();
};