// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef _INVULNERABLEBASES_PRIVATE
#define _INVULNERABLEBASES_PRIVATE

//#define DEBUG

#define DWORD	::DWORD
#define FLOAT	::FLOAT
#define UINT	::UINT
#define INT		::INT

#define PLUGIN_NAME "InvulnerableBases"
#define PLUGIN_VERSION "1.3.5"
#define PLUGIN_AUTHOR "Team ARK Bar"

#include <string>
#include <stdlib.h>
#include <Windows.h>
#include <string>
#include <vector>
#include <memory>
#include <cstdio>
#include <fstream>

using namespace std;

#include "API/AutoClosePtr.h"
#include "API/API.h"

#include "../Public/IInvulnerableBases.h"
#include "Core.h"
#include "Engine.h"

#include "Hook/MinHook.h"

#include "Hook/Pattern.h"
#include "Hook/Hook.h"

#include "Configuration/Configuration.h"

#include "Plugin/Plugin.h"

#endif